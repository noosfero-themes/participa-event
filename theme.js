(function($){
  function setBreadcrumbs(){
    $('div.breadcrumbs-plugin_content-breadcrumbs-block .separator').text(' / ');
    $('<span id="breadcrumbs-you-are-here">Você está aqui / </span>').insertBefore($('.breadcrumbs-plugin_content-breadcrumbs-block .block-inner-2').children().first());
  }

  function changeHighlighsBlock() {
    //Move HighlightsBlock title
    var title = $('.highlights-block .block-title');
    $(title).prependTo('.highlights-block .highlights-border');

    //Add span around HighlightsBlock label
    $('.highlights-block .highlights-label').each(function() {
      var label = $(this);
      label.html('<div>'+label.text()+'</div>');
    });
  }

  function addReadMoreLink() {
    $('#article.blog .blog-post').each(function() {
      var link = $(this).find('.title a');
      var div = '<a class="read-more-link" href="'+link.attr('href')+'"><span>leia mais</span></a>';
      $(div).insertAfter($(this).find('.article-compact-image'));
    });
  }

  function addReadAllLink() {
    if ($('body').hasClass('profile-homepage')) {
      var block = $('.box-1 .blocks > .block-outer > .display-content-block ');
      var link = $('#theme-header #read-all-link');
      $(link).appendTo(block);
    };
  }

  $(document).ready(function(){
    setBreadcrumbs();
    changeHighlighsBlock();
    addReadMoreLink();
    addReadAllLink();
  });
})(jQuery);
